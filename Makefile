all: HLV/coinc_inj_found.xml HLVI/coinc_inj_found.xml HLVIK/coinc_inj_found.xml HLV/coinc_inj_found.sqlite HLVI/coinc_inj_found.sqlite HLVIK/coinc_inj_found.sqlite

inj.xml:
	lalapps_inspinj -o $@ \
	--m-distr fixMasses --fixed-mass1 1.4 --fixed-mass2 1.4 \
	--t-distr uniform --time-step 600 \
	--gps-start-time $(shell lalapps_tconvert 2016-09-01) \
	--gps-end-time   $(shell lalapps_tconvert 2017-03-01) \
	--d-distr volume --min-distance 1 --max-distance 600e3 \
	--l-distr random --i-distr uniform \
	--f-lower 30 --waveform TaylorF2threePointFivePN --disable-spin

psd.xml:
	bayestar_sample_model_psd -o $@ --H1=aLIGOZeroDetHighPower --L1=aLIGOZeroDetHighPower --V1=AdvVirgo --K1=KAGRA --I1=aLIGOZeroDetHighPower

HLV/coinc.xml: psd.xml inj.xml
	mkdir -p $(@D) && cd $(@D) && \
	bayestar_realize_coincs --reference-psd ../psd.xml ../inj.xml -o $(@F) \
	--detector H1 L1 V1 --keep-subthreshold

HLVI/coinc.xml: psd.xml inj.xml
	mkdir -p $(@D) && cd $(@D) && \
	bayestar_realize_coincs --reference-psd ../psd.xml ../inj.xml -o $(@F) \
	--detector H1 L1 V1 I1 --keep-subthreshold

HLVIK/coinc.xml: psd.xml inj.xml
	mkdir -p $(@D) && cd $(@D) && \
	bayestar_realize_coincs --reference-psd ../psd.xml ../inj.xml -o $(@F) \
	--detector H1 L1 V1 I1 K1 --keep-subthreshold

%/coinc_inj.xml: %/coinc.xml inj.xml
	ligolw_add $^ -o $@

%/coinc_inj_found.xml: %/coinc_inj.xml
	lalapps_inspinjfind < $< > $@

%.sqlite: %.xml
	ligolw_sqlite --database $@ --preserve-ids $<
